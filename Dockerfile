ARG TF_VERSION

FROM quay.io/roboll/helmfile:v0.142.0 as helmfile

FROM hashicorp/terraform:${TF_VERSION}

ENV VAULT_URL=https://releases.hashicorp.com/vault/1.10.2/vault_1.10.2_linux_amd64.zip

RUN apk --no-cache add curl wget unzip python3 ca-certificates git bash curl jq

# helm
ARG HELM_VERSION="v3.7.0"
ARG HELM_SHA256="096e30f54c3ccdabe30a8093f8e128dba76bb67af697b85db6ed0453a2701bf9"
ARG HELM_LOCATION="https://get.helm.sh"
ARG HELM_FILENAME="helm-${HELM_VERSION}-linux-amd64.tar.gz"

RUN set -x && \
    wget ${HELM_LOCATION}/${HELM_FILENAME} && \
    echo Verifying ${HELM_FILENAME}... && \
    sha256sum ${HELM_FILENAME} | grep -q "${HELM_SHA256}" && \
    echo Extracting ${HELM_FILENAME}... && \
    tar zxvf ${HELM_FILENAME} && mv /linux-amd64/helm /usr/local/bin/ && \
    rm ${HELM_FILENAME} && rm -r /linux-amd64

# helm plugins
RUN helm plugin install https://github.com/databus23/helm-diff --version v3.1.3 && \
    helm plugin install https://github.com/jkroepke/helm-secrets --version v3.5.0 && \
    helm plugin install https://github.com/aslafy-z/helm-git.git --version v0.10.0

# helmfile copy
COPY --from=helmfile /usr/local/bin/helmfile /usr/local/bin/helmfile

# aws cli install
RUN curl --silent --location --output /tmp/awscli-bundle.zip "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" && \
    unzip -q -d /tmp/ /tmp/awscli-bundle.zip && \
    python3 /tmp/awscli-bundle/install -i /opt/aws -b /usr/local/bin/aws

# vault client install
RUN curl --silent --output /tmp/vault.zip ${VAULT_URL} && \
    unzip -o /tmp/vault.zip -d /usr/local/bin/ && \
    chmod 0755 /usr/local/bin/vault

# kubectl install
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin